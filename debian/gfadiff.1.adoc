= gfadiff(1)
:doctype: manpage

== NAME

gfadiff - compare two GFA files

== SYNOPSIS

*gfadiff* [-h] [-s] [-l] [-c] [-p] [-i] [-script] <gfa1> <gfa2>

== DESCRIPTION

If a combination of -h,-s,-l,-c and/or -p is specified, then
only record of the specified type [h=headers, s=segments,
l=links, c=containments, p=paths] are compared.
(default: -h -s -l -c -p)

== OTHER OPTIONS

*-i*::
  output message if identical

*-script*::
  create Ruby script to transform gfa1 into gfa2

== SEE ALSO

*rgfa-simdebruijn*(1), *rgfa-findcrispr*(1), *rgfa-mergelinear*(1)

== BUGS

Please report bugs to the RGFA issue tracker:
https://github.com/ggonnella/rgfa/issues

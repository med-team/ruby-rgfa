= rgfa-mergelinear(1)
:doctype: manpage

== NAME

rgfa-mergelinear - merge linear paths in assembly graphs

== SYNOPSIS

*rgfa-mergelinear* <gfa>

== DESCRIPTION

Merges linear paths in assembly graphs specified by a GFA file.

== SEE ALSO

*gfadiff*(1), *rgfa-findcrispr*(1), *rgfa-simdebruijn*(1)

== BUGS

Please report bugs to the RGFA issue tracker:
https://github.com/ggonnella/rgfa/issues
